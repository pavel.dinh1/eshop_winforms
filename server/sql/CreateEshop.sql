create database eshop;
use eshop;
drop database eshop;

create table adress
(
	id_adr int primary key identity(1,1),
	city nvarchar(50) not null,
	street nvarchar(50) not null,
	street_num nvarchar(50),
	postal_code int not null
);

create table loginUser
(
 id_user int primary key identity(1,1),
 username nvarchar(50),
 passwd nvarchar (50),
 userID int not null,
 firstName nvarchar(50) not null,
 lastName nvarchar(50) not null
);

create table product
(
id_prod int primary key identity(1,1),
prodName nvarchar(50) not null,
prodCode int,
prodDescription nvarchar(500),
price_per_prod money,
id_cat int,
id_branch int,
);

create table branch
(
id_branch int primary key identity(1,1),
branch_type nvarchar(10) not null check(branch_type = 'vedlej��' or branch_type = 'vedlejsi' or 
branch_type = 'hlavni' or branch_type = 'hlavn�'),
-- Adress
id_adr int,
);

create table stash
(
id_stash int primary key identity(1,1),
 -- loginUser
 id_logUsr int,
 -- Product
 id_prod int
);

create table category
(
id_cat int primary key identity(1,1),
catName nvarchar(50)
);

create table orderHistory
(
id_orHis int primary key identity(1,1),
dat_order date,

-- orders
id_orders int,

-- user
id_user int,

-- product
id_prod int,

);

create table orders
(
id_order int primary key identity(1,1),
orderStatus nvarchar(20) not null,

-- stash
id_stash int,

-- login
id_login int,

);

create table distributions
(
id_dist int primary key identity(1,1),
-- branch
id_branch int,
);

alter table stash 
add foreign key(id_logUsr) references loginUser(id_user);

alter table stash
add foreign key(id_prod) references product(id_prod);

alter table branch
add foreign key(id_adr) references adress(id_adr);

alter table product
add foreign key(id_branch) references branch(id_branch)

alter table product
add foreign key(id_cat) references category(id_cat)

alter table orderHistory
add foreign key(id_orders) references orders(id_order)

alter table orderHistory
add foreign key(id_user) references loginUser(id_user)

alter table orderHistory
add foreign key(id_prod) references product(id_prod)

alter table orders
add foreign key(id_stash) references stash(id_stash)

alter table orders
add foreign key(id_login) references loginUser(id_user)

alter table distributions
add foreign key(id_branch) references branch(id_branch)
