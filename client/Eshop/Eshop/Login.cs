﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Eshop
{
 public partial class Login : Form
 {
  public Login()
  {
   InitializeComponent();
  }

  private void button1_Click(object sender, EventArgs e)
  {
   string CommandText = "select username,passwd from loginuser where username = @user and passwd = @password";
   string returnValue = "";
   try
   {
    Catalog cat = new Catalog();

    using (SqlConnection conn = Helper.GetInstance())
    {
     using (SqlCommand com = new SqlCommand(CommandText, conn))
     {
      com.Parameters.Add("@user", SqlDbType.NVarChar).Value = LoginText.Text;
      com.Parameters.Add("@password", SqlDbType.NVarChar).Value = PasswdText.Text;
      conn.Open();
      returnValue = (string)com.ExecuteScalar();
     }
    }
    if (String.IsNullOrEmpty(returnValue))
    {
     MessageBox.Show("Invalid username or password !");
     return;
    }
    else
    {
     this.Hide();
     cat.ShowDialog();
     this.Close();
    }
   }
   catch (Exception ex)
   {
    MessageBox.Show("Connection was unsuccessful !");
    throw new Exception(ex.Message);
   }
  }

  private void button2_Click(object sender, EventArgs e)
  {
   Register reg = new Register();
   this.Hide();
   reg.ShowDialog();
   this.Close();
  }
 }
}
