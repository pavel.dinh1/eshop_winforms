﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eshop
{
 class User
 {
  public string FirstName { get; set; }
  public string LastName { get; set; }
  public string UserName { get; set; }
  public string Password { get; set; }
  public string Email { get; set; }

  public User()
  {
   FirstName = "none";
   LastName = "none";
   UserName = "none";
   Password = "";
   Email = "";
  }
  public User(string firstname, string lastname, string username, string password, string email)
  {
   FirstName = firstname;
   LastName = lastname;
   UserName = username;
   Password = password;
   Email = email;
  }
 }
}
