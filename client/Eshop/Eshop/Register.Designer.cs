﻿namespace Eshop
{
 partial class Register
 {
  /// <summary>
  /// Required designer variable.
  /// </summary>
  private System.ComponentModel.IContainer components = null;

  /// <summary>
  /// Clean up any resources being used.
  /// </summary>
  /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
  protected override void Dispose(bool disposing)
  {
   if (disposing && (components != null))
   {
    components.Dispose();
   }
   base.Dispose(disposing);
  }

  #region Windows Form Designer generated code

  /// <summary>
  /// Required method for Designer support - do not modify
  /// the contents of this method with the code editor.
  /// </summary>
  private void InitializeComponent()
  {
   this.button1 = new System.Windows.Forms.Button();
   this.label1 = new System.Windows.Forms.Label();
   this.textBox1 = new System.Windows.Forms.TextBox();
   this.textBox2 = new System.Windows.Forms.TextBox();
   this.label2 = new System.Windows.Forms.Label();
   this.textBox3 = new System.Windows.Forms.TextBox();
   this.label3 = new System.Windows.Forms.Label();
   this.textBox4 = new System.Windows.Forms.TextBox();
   this.label4 = new System.Windows.Forms.Label();
   this.textBox5 = new System.Windows.Forms.TextBox();
   this.label5 = new System.Windows.Forms.Label();
   this.textBox6 = new System.Windows.Forms.TextBox();
   this.label6 = new System.Windows.Forms.Label();
   this.label7 = new System.Windows.Forms.Label();
   this.label8 = new System.Windows.Forms.Label();
   this.button2 = new System.Windows.Forms.Button();
   this.SuspendLayout();
   // 
   // button1
   // 
   this.button1.Location = new System.Drawing.Point(289, 265);
   this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
   this.button1.Name = "button1";
   this.button1.Size = new System.Drawing.Size(100, 28);
   this.button1.TabIndex = 0;
   this.button1.Text = "Register";
   this.button1.UseVisualStyleBackColor = true;
   this.button1.Click += new System.EventHandler(this.button1_Click);
   // 
   // label1
   // 
   this.label1.AutoSize = true;
   this.label1.Location = new System.Drawing.Point(64, 62);
   this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
   this.label1.Name = "label1";
   this.label1.Size = new System.Drawing.Size(76, 17);
   this.label1.TabIndex = 1;
   this.label1.Text = "First Name";
   // 
   // textBox1
   // 
   this.textBox1.Location = new System.Drawing.Point(145, 58);
   this.textBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
   this.textBox1.Name = "textBox1";
   this.textBox1.Size = new System.Drawing.Size(243, 23);
   this.textBox1.TabIndex = 2;
   // 
   // textBox2
   // 
   this.textBox2.Location = new System.Drawing.Point(145, 90);
   this.textBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
   this.textBox2.Name = "textBox2";
   this.textBox2.Size = new System.Drawing.Size(243, 23);
   this.textBox2.TabIndex = 4;
   // 
   // label2
   // 
   this.label2.AutoSize = true;
   this.label2.Location = new System.Drawing.Point(64, 94);
   this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
   this.label2.Name = "label2";
   this.label2.Size = new System.Drawing.Size(76, 17);
   this.label2.TabIndex = 3;
   this.label2.Text = "Last Name";
   // 
   // textBox3
   // 
   this.textBox3.Location = new System.Drawing.Point(145, 122);
   this.textBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
   this.textBox3.Name = "textBox3";
   this.textBox3.Size = new System.Drawing.Size(243, 23);
   this.textBox3.TabIndex = 6;
   // 
   // label3
   // 
   this.label3.AutoSize = true;
   this.label3.Location = new System.Drawing.Point(64, 126);
   this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
   this.label3.Name = "label3";
   this.label3.Size = new System.Drawing.Size(42, 17);
   this.label3.TabIndex = 5;
   this.label3.Text = "Email";
   // 
   // textBox4
   // 
   this.textBox4.Location = new System.Drawing.Point(145, 154);
   this.textBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
   this.textBox4.Name = "textBox4";
   this.textBox4.Size = new System.Drawing.Size(243, 23);
   this.textBox4.TabIndex = 8;
   // 
   // label4
   // 
   this.label4.AutoSize = true;
   this.label4.Location = new System.Drawing.Point(64, 158);
   this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
   this.label4.Name = "label4";
   this.label4.Size = new System.Drawing.Size(73, 17);
   this.label4.TabIndex = 7;
   this.label4.Text = "Username";
   // 
   // textBox5
   // 
   this.textBox5.Location = new System.Drawing.Point(145, 186);
   this.textBox5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
   this.textBox5.Name = "textBox5";
   this.textBox5.Size = new System.Drawing.Size(243, 23);
   this.textBox5.TabIndex = 10;
   this.textBox5.UseSystemPasswordChar = true;
   // 
   // label5
   // 
   this.label5.AutoSize = true;
   this.label5.Location = new System.Drawing.Point(64, 190);
   this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
   this.label5.Name = "label5";
   this.label5.Size = new System.Drawing.Size(69, 17);
   this.label5.TabIndex = 9;
   this.label5.Text = "Password";
   // 
   // textBox6
   // 
   this.textBox6.Location = new System.Drawing.Point(145, 218);
   this.textBox6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
   this.textBox6.Name = "textBox6";
   this.textBox6.Size = new System.Drawing.Size(243, 23);
   this.textBox6.TabIndex = 12;
   this.textBox6.UseSystemPasswordChar = true;
   // 
   // label6
   // 
   this.label6.AutoSize = true;
   this.label6.Location = new System.Drawing.Point(15, 222);
   this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
   this.label6.Name = "label6";
   this.label6.Size = new System.Drawing.Size(120, 17);
   this.label6.TabIndex = 11;
   this.label6.Text = "Confirm password";
   // 
   // label7
   // 
   this.label7.AutoSize = true;
   this.label7.Location = new System.Drawing.Point(328, 23);
   this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
   this.label7.Name = "label7";
   this.label7.Size = new System.Drawing.Size(61, 17);
   this.label7.TabIndex = 13;
   this.label7.Text = "Register";
   // 
   // label8
   // 
   this.label8.AutoSize = true;
   this.label8.Location = new System.Drawing.Point(64, 11);
   this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
   this.label8.Name = "label8";
   this.label8.Size = new System.Drawing.Size(86, 17);
   this.label8.TabIndex = 14;
   this.label8.Text = "Dinh E-shop";
   // 
   // button2
   // 
   this.button2.Location = new System.Drawing.Point(145, 265);
   this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
   this.button2.Name = "button2";
   this.button2.Size = new System.Drawing.Size(100, 28);
   this.button2.TabIndex = 15;
   this.button2.Text = "Back";
   this.button2.UseVisualStyleBackColor = true;
   this.button2.Click += new System.EventHandler(this.button2_Click);
   // 
   // Register
   // 
   this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
   this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
   this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
   this.ClientSize = new System.Drawing.Size(501, 343);
   this.Controls.Add(this.button2);
   this.Controls.Add(this.label8);
   this.Controls.Add(this.label7);
   this.Controls.Add(this.textBox6);
   this.Controls.Add(this.label6);
   this.Controls.Add(this.textBox5);
   this.Controls.Add(this.label5);
   this.Controls.Add(this.textBox4);
   this.Controls.Add(this.label4);
   this.Controls.Add(this.textBox3);
   this.Controls.Add(this.label3);
   this.Controls.Add(this.textBox2);
   this.Controls.Add(this.label2);
   this.Controls.Add(this.textBox1);
   this.Controls.Add(this.label1);
   this.Controls.Add(this.button1);
   this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
   this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
   this.Name = "Register";
   this.Text = "Register";
   this.ResumeLayout(false);
   this.PerformLayout();

  }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label6;
  private System.Windows.Forms.Label label7;
  private System.Windows.Forms.Label label8;
  private System.Windows.Forms.Button button2;
 }
}