﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Eshop
{
 public static class Helper
 {
  private static SqlConnection instance = null;
  
  /// <summary>
  /// Getting connection String from App.config
  /// </summary>
  /// <param name="name">name of the connection string in App.Config</param>
  /// <returns>App.config connection string</returns>
  public static string ConnValue(string name)
  {
   return ConfigurationManager.ConnectionStrings[name].ConnectionString;
  }

  /// <summary>
  /// Made an instance of sql connection once
  /// </summary>
  /// <returns>SqlConnection instance</returns>
  public static SqlConnection GetInstance()
  {
   if(instance == null)
   {
    instance = new SqlConnection(ConnValue("eshop"));
   }
   return instance;
  }
 }
}
